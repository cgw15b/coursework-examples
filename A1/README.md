> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development 

## Casey G. Woods 

### Assignment # 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket 
2. Development Installations 
3. Chapter Questions (Chs 1, 2)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation 
* Screenshot of running java Hello;
* Screenshot of running Android Studio - My First App 
* Git commands with short descriptions;
* Bitbucket repo links: a.) this assignment and b.) the completed tutorials above (bitbucketstationlocations and myteamquotes).

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - creates a new git repository 
2. git status -  shows state of working directory and staging area
3. git add - creates a change in the working directory 
4. git commit - creates a new commit object, sets branch to point at new commit
5. git push - -ushes all the modified local objects to the remote repository
6. git pull - fetches files from the remote repository and merges it with local one
7. git reset - resets your index and working directory to the state of your last commit

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/Screenshot_AMPPS.png) 

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/javahello.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android_hellocaseywoods.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
