> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Casey Woods

### Assignment #5 Requirements:

*Objective:*

1. Create form for user entered data that uses server-side validation. 
2. Link created MySQL database to populate tables in local host.
3. Turn off client side validation, show error page.
4. Display data through created php local site


#### Assignment Screenshots:

*Screenshot of index.php main page*:

![A5.Index.php](img/a5_screenshot1.png)

*Screenshot of failed validation*: 

![add_petstore_process.php (includes error.php)](img/a5_screenshot2.png)





