> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Casey Woods

### Assignment #4 Requirements:

*Objective:*

1. Create a local host website showcasing our recent achievements. 
2. Work with client-side validation to show wheter the selected info is valid or not. 

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of main portal;
* Screenshot of failed validation;
* Screenshot of passed validation;


#### Assignment Screenshots:

*Screenshot of portal main page*:

![ERD Screenshot](img/coverphoto2.png)

*Screenshot of failed validation*: 

![First User Interface](img/failedvalidation2.png)

*Screenshot of passed validation*:

![Second User Interface](img/passedvalidation2.png)



