> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Casey Woods

### Project 2 Requirements:

*Objective:*

1. Create form to update records in a database using server-side validation
2. Create server-side process for deleting records in a database
3. Update website homepage with new pictures
4. Create a page that displays a RSS Feed


#### Assignment Screenshots:

*Screenshot of index.php main page*:

![p2_1](img/p2_Screenshot1.png)

*Screenshot of edit_petstore.php*: 

![p2_2](img/p2_Screenshot2.png)

*Screenshot of error.php*: 

![p2_3](img/p2_Screenshot3.png)

*Screenshot of Carousel*:

![p2_4](img/p2_Screenshot4.png)

*Screenshot of RSS Feed*: 

![p2_5](img/p2_Screenshot5.png)





