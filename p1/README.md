> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Casey Woods

### Project #1 Requirements:

*Objective:*

1. Create a buisness card using Android Studio. 
2. Create a launcher icon image and display it in both activities (screens)
3. Must add background color(s) to both activities
4. Must add border around image and button
5. Must add text shadow (button)

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application’s first user interface;
* Screenshot of running application’s second user interface;


#### Assignment Screenshots:

*Screenshot of running application’s first user interface*: 

![First User Interface](img/p1-screenshot1.png)

*Screenshot of running application’s second user interface*:

![Second User Interface](img/p1-screenshot2.png)


