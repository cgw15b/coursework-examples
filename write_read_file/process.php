<!DOCTYPE html>
<html lang="en">
<head>
<!--
				 "Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Assignment 5 - Server Side Validation">
	<meta name="author" content="Casey Woods">
	<link rel="icon" href="favicon.ico">

	<title>Write/Read File</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>
    <?php include_once("global/header.php");?>
    
    <div class="container">
        <div class="starter-template">
            <div class="page-header">
                <?php include_once("global/header.php");?>
            </div>
            <p class="text-justify">
            <?php

                $myfile = fopen("file.txt","w+")or exit("Unable to open file!");
                $txt = $_POST['comment'];
                fwrite($myfile, $txt);
                fclose($myfile);

                $myfile = fopen("file.txt","r+")or exit("Unable to open file!");
                while(!feof($myfile)) {
                    echo fgets($myfile) . "<br />";
                }
                
                fclose($myfile);
            ?>
            </p>
</body>


