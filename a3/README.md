> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Casey Woods

### Assignment #3 Requirements:

*Objective:*

1. Create an ERD in MySQL that follows companies buisness rules. 
2. Create a mobile ticket pricing app in Android Studio.

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of ERD;
* Screenshot of running application’s first user interface;
* Screenshot of running application’s second user interface;


#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD Screenshot](img/a3-ERD.png)

*Screenshot of running application’s first user interface*: 

![First User Interface](img/a3-Screenshot1.png)

*Screenshot of running application’s second user interface*:

![Second User Interface](img/a3-Screenshot2.png)


