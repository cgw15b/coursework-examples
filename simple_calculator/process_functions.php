<!DOCTYPE html>
<html lang="en">
<head>
<!--
				 "Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Assignment 5 - Server Side Validation">
	<meta name="author" content="Evan Pollak">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Assignment5</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>
    <?php include_once("global/header.php");?>
    
    <div class="container">
        <div class="starter-template">
            <div class="page-header">
                <?php include_once("global/header.php");?>
            </div>

            <?php

                if (!empty($_POST))
                {
                    $num1 = $_POST['num1'];
                    $num2 = $_POST['num2'];
                    $operation = $_POST['operation'];

                    if(preg_match('/^[-+]?[0-9]*\.?[0-9]+$/', $num1) && preg_match('/^[-+]?[0-9]*\.?[0-9]+$/', $num2))
                    {
                        echo '<h2>'."$operation".'</h2>';

                        switch($operation)
                        {
                            case"addition":
                                echo "$num1"." + "."$num2"." = ";
                                echo $num1 + $num2;
                                break;

                            case"subtraction":
                                echo "$num1"." - "."$num2"." = ";
                                echo $num1 - $num2;
                                break;

                            case"multiplication":
                                echo "$num1"." * "."$num2"." = ";
                                echo $num1 * $num2;
                                break;

                            case"division":
                                if($num2 == 0)
                                {
                                    echo "cannot divide by zero";
                                }
                                else
                                {
                                    echo "$num1"." / "."$num2"." = ";
                                    echo $num1 / $num2;
                                }
                                break;

                                case"exponentiation":
                                    echo "$num1"." raised to the power of "."$num2"." = ";
                                    echo pow($num1, $num2);
                                break;

                                default:
                                echo "Must select and operation!";
                        }

                    }


                }
            ?>
</body>


