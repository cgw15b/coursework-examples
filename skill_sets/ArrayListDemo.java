import java.util.Scanner;
import java.util.*;

public class ArrayListDemo
{
    public static void main(String args[])
    {
        System.out.println("Program populates ArrayList of strings with user-entered animal type vailes.");
        System.out.println("Examples: Polar bear, Guinea pig, dog, cat, bird.");
        System.out.println("Program continues to collect user-entered values until user types \"n\".");
        System.out.println("Program displays ArrayList values after each iteration");

        System.out.println("");

        //create program variables/objects
        //create Scanner object
        Scanner sc = new Scanner(System.in);
        ArrayList<String> obj = new ArrayList<String>(); //create string type arraylist
        String myStr = "";
        String choice = "y";
        int num = 0;

        while (choice.equals("y"))
        {
            System.out.print("Enter animal type: ");
            myStr = sc.nextLine();
            obj.add(myStr); //add string object
            num = obj.size();
            System.out.println("ArrayList elements:" + obj + "\nArrayList Size = " + num);
            System.out.print("\nContinue? Enter y or n: ");
            choice = sc.next();
            sc.nextLine();
        }

        
    }
}