> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 

## Casey G. Woods

### LIS4381  Requirements:

*Course Work Links:*

1 [a1 README.md](https://bitbucket.org/cgw15b/lis4381/src/master/A1/README.md)
    
* Install .NET Core
* Create hwapp application
* Create aspnetcoreapp application 
* Provide screenshots of installations 
* Create Bitbucket Repo
* Complete Bitbucket Tutorials 
* Provide git command descriptions 

2 [a2 README.md](https://bitbucket.org/cgw15b/lis4381/src/master/a2/README.md)
    
* Create a running application on Android Studio 
* Ensured that the application was able to run 2 activites 
* Change background color of application 

3 [a3 README.md](https://bitbucket.org/cgw15b/lis4381/src/master/a3/README.md)
    
* Create an ERD in MySQL that follows companies buisness rules.
* Create a mobile ticket pricing app in Android Studio.
* Change ackground color 
* Ass a launcher icon (favicon)

4 [p1 README.md](https://bitbucket.org/cgw15b/lis4381/src/master/p1/README.md)

* Create a buisness card using Android Studio. 
* Create a launcher icon image and display it in both activities (screens)
* Must add background color(s) to both activities
* Must add border around image and button
* Must add text shadow (button)

5 [a4 README.md](https://bitbucket.org/cgw15b/lis4381/src/master/a4/)

* Create a local host website showcasing our recent achievements
* Work with client-side validation to show wheter the selected info is valid or not. 
* Carsoul must contain 3 images
* Link to local lis4381 web app

6 [a5 README.md](https://bitbucket.org/cgw15b/lis4381/src/master/a5/)

* Create form for user entered data that uses server-side validation.
* Link created MySQL database to populate tables in local host.
* Turn off client side validation, show error page.
* Display data through created php local site

7 [p2 README.md](https://bitbucket.org/cgw15b/lis4381/src/master/p2/)

* Create form to update records in a database using server-side validation
* Create server-side process for deleting records in a database
* Update website homepage with new pictures
* Create a page that displays a RSS Feed